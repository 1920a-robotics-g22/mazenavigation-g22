#function for getting binary maze from one frame of webcam feed
#might need to lower resolution for calculation and then upscale the resolution coordinates

import numpy as np
import cv2
import os
from pathfinding.core.diagonal_movement import DiagonalMovement
from pathfinding.core.grid import Grid
from pathfinding.finder.breadth_first import BreadthFirstFinder

def maze2binary(frame):
    valuesfile = "maze_values.txt"
    if os.path.exists(valuesfile) and os.path.getsize(valuesfile) > 0:
        with open(valuesfile, "r") as configvalues:      
            lines = configvalues.readlines()
            lowthreshold = int(lines[0])
            dilationsize = int(lines[1])
            erosionsize = int(lines[2])     
    else:
        lowthreshold = 50
        dilationsize = 23
        erosionsize = 23
    
    #convert to grayscale
    gray_scale = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    #blur
    img_blur = cv2.GaussianBlur(gray_scale,(5,5),0)
    
    #Canny edge detection
    detected_edges = cv2.Canny(img_blur, lowthreshold, lowthreshold*3, 3)
    
    #morphological operations
    dilate_mask = np.ones((dilationsize, dilationsize), np.uint8)
    dilate_result = cv2.dilate(detected_edges, dilate_mask, iterations = 1)
    
    erode_mask = np.ones((erosionsize, erosionsize), np.uint8)
    erosion_result = cv2.erode(dilate_result, erode_mask, iterations = 1)
    
    #inverts black and white
    img = cv2.bitwise_not(erosion_result)
    
    #convert array range to be from 0 to 1 (default is 0 to 255)
    floatvalues = img / 255

    #convert elements to integers, 0 as walls and 1 as passable area
    mazearray = floatvalues.astype(int)
    
    mazenumpy = np.array(mazearray)
    
    h, w = mazenumpy.shape
    outputtxt = ""
    for r in range(h):
        for c in range(w):
            element = mazenumpy.item(r,c)
            outputtxt+=str(element)
        outputtxt+="\n"
    return outputtxt

def get_turning_points(pathlist):
    outlist=[]
    outlist.append((pathlist[0], get_starting_destination(pathlist)))
    for i in range(len(pathlist)):
        try:
            if pathlist[i][1]==pathlist[i-1][1] and pathlist[i][1]<pathlist[i+1][1]:
                outlist.append((pathlist[i], 180))
            elif pathlist[i][1]==pathlist[i-1][1] and pathlist[i][1]>pathlist[i+1][1]:
                outlist.append((pathlist[i], 0))
            elif pathlist[i][0]==pathlist[i-1][0] and pathlist[i][0]>pathlist[i+1][0]:
                outlist.append((pathlist[i], 90))
            elif pathlist[i][0]==pathlist[i-1][0] and pathlist[i][0]<pathlist[i+1][0]:
                outlist.append((pathlist[i], -90))
        except:
            pass
    return outlist

def get_starting_destination(pathlist):
    if pathlist[0][0]>pathlist[1][0]:
        return 90
    elif pathlist[0][1]>pathlist[1][1]:
        return 0
    elif pathlist[0][0]<pathlist[1][0]:
        return -90
    else:
        return 180
    
def straighten_corners(pathlist):
    for i in range(len(pathlist)):
        try:
            if pathlist[i-2][0]<pathlist[i][0] and pathlist[i-2][1]>pathlist[i][1] and pathlist[i+2][0]>pathlist[i][0] and pathlist[i+2][1]<pathlist[i][1]:
                pathlist[i]=(pathlist[i][0]+1,pathlist[i][1]+1)
            elif pathlist[i-2][0]>pathlist[i][0] and pathlist[i-2][1]<pathlist[i][1] and pathlist[i+2][0]<pathlist[i][0] and pathlist[i+2][1]>pathlist[i][1]:
                pathlist[i]=(pathlist[i][0]+1,pathlist[i][1]+1)
            elif pathlist[i-2][0]>pathlist[i][0] and pathlist[i-2][1]>pathlist[i][1] and pathlist[i+2][0]<pathlist[i][0] and pathlist[i+2][1]<pathlist[i][1]:
                pathlist[i]=(pathlist[i][0]-1,pathlist[i][1]-1)
            elif pathlist[i-2][0]<pathlist[i][0] and pathlist[i-2][1]<pathlist[i][1] and pathlist[i+2][0]>pathlist[i][0] and pathlist[i+2][1]>pathlist[i][1]:
                pathlist[i]=(pathlist[i][0]-1,pathlist[i][1]-1)
        except:
            pass

def mazesolve(maze, start, end):

    maze=maze.strip().split('\n')

    matrix=[]
    for i in maze:
        matrix.append(list(i))

    grid = Grid(matrix=matrix)
    start = grid.node(int(start[1]),int(start[0]))
    end = grid.node(end[1], end[0])

    finder = BreadthFirstFinder(diagonal_movement=DiagonalMovement.never)
    path, runs = finder.find_path(start, end, grid)
    straighten_corners(path)
    return get_turning_points(path)

