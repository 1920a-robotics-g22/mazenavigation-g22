import numpy as np
import cv2
import easygopigo3 as go
import time

myRobot = go.EasyGoPiGo3()
myRobot.set_speed(40)

try:
  file = open('trackbar_defaults.txt','r')
  values = []

  for line in file:
    values.append(int(line))
    
  lH = values[0]
  lS = values[1]
  lV = values[2]
  hH = values[3]
  hS = values[4]
  hV = values[5]

except:

  lH = 125
  lS = 125
  lV = 125
  hH = 255
  hS = 255
  hV = 255

# open the camera
cap = cv2.VideoCapture(0)
def updateValueh(newh):

  global lH
  lH=newh
  
  return
def updateValues(news):

  global lS
  lS=news

  return

def updateValuev(newv):

  global lV
  
  lV=newv
  
  return
def updateValueH(newH):

  global hH
 
  hH=newH
  
  return
def updateValueS(newS):

  global hS
 
  hS=newS
  
  return
def updateValueV(newV):

  global  hV
  
  hV=newV
  return


    
blobparams = cv2.SimpleBlobDetector_Params()

blobparams.filterByArea = True
blobparams.filterByCircularity = False
blobparams.filterByInertia = False
blobparams.filterByConvexity = False
blobparams.minDistBetweenBlobs = 200
blobparams.minArea = 5000 #so it will not detect blobs smaller than 1000
blobparams.maxArea = 30000 #will detect the bigger circles roughly 100x100 pixels and a little extra


detector = cv2.SimpleBlobDetector_create(blobparams)

cv2.namedWindow("Output")

# Attach a trackbar to a window named 'Output'
cv2.createTrackbar("lH", "Output", lH, 255, updateValueh)
cv2.createTrackbar("lS", "Output", lS, 255, updateValues)
cv2.createTrackbar("lV", "Output", lV, 255, updateValuev)
cv2.createTrackbar("hH", "Output", hH, 255, updateValueH)
cv2.createTrackbar("hS", "Output", hS, 255, updateValueS)
cv2.createTrackbar("hV", "Output", hV, 255, updateValueV)

#init coordinates of blob
xKey=0 #x coordinate of keypoint
yKey=0 #y coordinate of keypoint
state=False
i=0 #for next command in comands list and next position in positions list
while True:
  
 
  #read the image from the camera
  ret, frame = cap.read()

  frame = cv2.cvtColor(frame,cv2.COLOR_BGR2HSV) #BGR to HSV

  
  lowerLimits = np.array([lH, lS, lV])
  upperLimits = np.array([hH, hS, hV])

  # Our operations on the frame come here
  thresholded = cv2.inRange(frame, lowerLimits, upperLimits)
  outimage = cv2.bitwise_and(frame, frame, mask = thresholded)

  
  thresholded = cv2.bitwise_not(thresholded)
  cv2.imshow('Output', thresholded)
  keypoints = detector.detect(thresholded)
  imgcop=frame.copy()
  imgcop = cv2.drawKeypoints(imgcop, keypoints, np.array([]), (0,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

  if len(keypoints) > 0:
        for keypoint in keypoints:
            coord=keypoint.pt
            coord=list(coord)
            xKey = coord[0]
            yKey = coord[1]
            cv2.putText(imgcop, "x:" + str(int(xKey))+" y:"+ str(int(yKey)), (int(xKey),int(yKey)), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2)
           
  cv2.imshow('Original', imgcop)
  
  ##########################################################
  ##########################################################
  ###################PART FOR MOVEMENTS#####################
  ##########################################################
  ##########################################################
  # exmple setpoints from algorithm x(first) and y(second) coordinate
  setPoints = [300, 240, 50, 140]
  #to extract coordinates from list
  #setPoints[0::2]  - first elements from 0
  #setPoints[1::2]  -second elements from 1
  # Convert Integral list to tuple list 
  # using zip() + list() 
  points = list(zip(setPoints[0::2],setPoints[1::2])) #[(300, 240), (50, 140)]

 # for adressing each setpoints coordinates xSet and ySet of where we want to be
  xy=points[i] # i.e (300, 240) if i ==0
  
  xSet=xy[0] # i.e 300 if i ==0
  ySet=xy[1] # i.e 240 if i ==0

  commands = ["left", "right", "forw"] #probably not going to be string but just to try
  if commands[i]=="left" and state==False:
      print("left 90 degrees")
      myRobot.left()
      time.sleep(8)
      state = True
      
  if commands[i]=="right" and state==False:
      print("right 90 degrees")
      myRobot.right()
      time.sleep(8)
      state = True
      
      #if keyPoint is close enough to setPoint
      # not very good algorithm need to tink of something else to
      #know for sure if the robot is actually in the location we want it to be
      #maybe using markers
  if state==True and abs(xSet-xKey)<10 or abs(ySet-yKey)<10:
      state = False #if robot has reached a desired position
      i = i +1      #will take next command and next position
  if state == True:
      myRobot.forward()
  
  
  # Quit the program when 'q' is pressed
  if cv2.waitKey(1) & 0xFF == ord('q'):
    file = open('trackbar_defaults.txt','w+')
    
    file.write(str(lH) + "\n")
    file.write(str(lS) + "\n")
    file.write(str(lV) + "\n")
    file.write(str(hH) + "\n")
    file.write(str(hS) + "\n")
    file.write(str(hV) + "\n")
    file.close() 
    break

# When everything done, release the capture
myRobot.stop()
print('closing program')
cap.release()
cv2.destroyAllWindows()


