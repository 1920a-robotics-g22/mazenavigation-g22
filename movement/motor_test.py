import cv2
import cv2.aruco as aruco
import math
import easygopigo3 as go
import time
x=0
y=0
state=False #state is a StateVariable which shows if robot is in the right direction for moving forward
iPoints=0 #for next command in comands list and next position in positions list
i=0
myRobot = go.EasyGoPiGo3()
myRobot.set_speed(40)
cap = cv2.VideoCapture(0)
degs=0
first_motor_speed=60
second_motor_speed=60 
  
#could be bigger than frame for maze solution but in later phase must be made identical
#(bigger is better for this program and controlling movement)
cap.set(cv2.CAP_PROP_FRAME_WIDTH, 640)
cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 360)

while True:

    ret, frame = cap.read()

    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY) #can use the same frame as used in maze to txt (definitely without blur!)
    aruco_dict = aruco.Dictionary_get(aruco.DICT_4X4_50) #4x4 should give better result than 6x6 because squares are bigger
    parameters =  aruco.DetectorParameters_create()

    #aruco marker detector
    corners, ids, rejectedImgPoints = aruco.detectMarkers(gray, aruco_dict, parameters=parameters)
    #to show corner coordinate arrays if needed
    #print(corners)
    
    if len(corners) > 0:
        #calculating center coordinates
       # x = (corners[0][0][0][0] + corners[0][0][1][0] + corners[0][0][2][0] + corners[0][0][3][0]) / 4
       # y = (corners[0][0][0][1] + corners[0][0][1][1] + corners[0][0][2][1] + corners[0][0][3][1]) / 4
        
        #or use int values:
        x = int((corners[0][0][0][0] + corners[0][0][1][0] + corners[0][0][2][0] + corners[0][0][3][0]) / 4)
        y = int((corners[0][0][0][1] + corners[0][0][1][1] + corners[0][0][2][1] + corners[0][0][3][1]) / 4)
        
        print("x: " + str(x) + " y: " + str(y))
        
        #choosing corners for angle calculation, here are two bottom ones so that robot facing north gives 0 degrees
        p4x = corners[0][0][3][0]
        p3x = corners[0][0][2][0]
        p4y = corners[0][0][3][1]
        p3y = corners[0][0][2][1]
        
        #angle/heading calculation
        dx = p3x - p4x
        dy = p4y - p3y
        
        rads = math.atan2(dy,dx)
        degs = math.degrees(rads)
        #heading north - 0 (+-5 is -5 to 5
        #heading east - -90 (+-5 is -95 to -85)
        #heading west - 90 (+-5 is 85 to 95)
        #heading south - 180 (+-5 is -175 to 175!!!
                #note the transition from east to south is -179 -> 180)
        print(int(degs))
    
    setPoints = [(200, 150 ), (200, 300)]
    #to extract coordinates from list
          #setPoints[0::2]  - first elements from 0
          #setPoints[1::2]  -second elements from 1
          # Convert Integral list to tuple list 
          # using zip() + list()
   # points = list(zip(setPoints[0::2],setPoints[1::2])) #[(300, 240), (50, 140)]
    points =[(200, 150), (200, 300)]

 # for adressing each setpoints coordinates xSet and ySet of where we want to be
    xy=points[i] # i.e (300, 240) if i ==0
  
    xSet=xy[0] # i.e 300 if i ==0
    ySet=xy[1] # i.e 240 if i ==0
 
    commands = [90, 180, 90, 90] #degrees of where we need to be
    if degs<100:
        first_motor_speed=50
        second_motor_speed=0
    myRobot.set_motor_dps(myRobot.MOTOR_LEFT, first_motor_speed)
    myRobot.set_motor_dps(myRobot.MOTOR_RIGHT, second_motor_speed)
  
    grayscale = aruco.drawDetectedMarkers(gray, corners)
    cv2.imshow("frame",grayscale)
    
    if cv2.waitKey(1) & 0xFF == ord('q'):
        first_motor_speed=0
        second_motor_speed=0
        break


cap.release()
cv2.destroyAllWindows()


