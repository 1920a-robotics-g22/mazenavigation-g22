import cv2
import numpy as np
import sys

np.set_printoptions(threshold=sys.maxsize)
low = 80
scale_percent = 40

def updatelow(new_value):
  global low
  low = new_value
  return

def updatescale(new_value):
  global scale_percent
  scale_percent = new_value
  return
  
cv2.namedWindow("Trackbars")
cv2.createTrackbar("low", "Trackbars", low, 255, updatelow)
cv2.createTrackbar("scale_percent", "Trackbars", scale_percent, 100, updatescale)

img = cv2.imread('maze.jpg')

while True:
    
    #downscaling the image
    width = int(img.shape[1] * scale_percent / 100)
    height = int(img.shape[0] * scale_percent / 100)
    dim = (width, height)
    scaled = cv2.resize(img, dim, interpolation = cv2.INTER_AREA)

    #convert to grayscale
    grayscale = cv2.cvtColor(scaled, cv2.COLOR_BGR2GRAY)
    #cv2.imshow('Grayscale', grayscale)
        
    #adding blur
    blurred = cv2.GaussianBlur(grayscale,(5,5),0)
    #cv2.imshow('Blurred', blurred)
    
    #thresholding
    ret, thresh = cv2.threshold(blurred, low, 255, cv2.THRESH_BINARY_INV)
    #cv2.imshow('Thresholded', thresh)
 
    #morphological operations
    morph_kernel = np.ones((19, 19), np.uint8)
    morphed = cv2.morphologyEx(thresh, cv2.MORPH_CLOSE, morph_kernel)
    cv2.imshow('Dilation and erosion', morphed)
    
    #convert array range to be from 0 to 1 (default is 0 to 255)
    floatvalues = morphed / 255

    #convert elements to integers, 0 as walls and 1 as passable area
    mazearray = floatvalues.astype(int)
    #print to file, use when done playing with trackbars and set those values as global variables before opening the program with this line uncommented    
    #print(mazearray, file=open('array.txt', 'w'))
 
    #quit when 'q' is pressed
    if cv2.waitKey(1) & 0xFF == ord('q'):
      break

cv2.destroyAllWindows()
