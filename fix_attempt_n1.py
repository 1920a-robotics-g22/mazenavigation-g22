from pathfinding.core.diagonal_movement import DiagonalMovement
from pathfinding.core.grid import Grid
from pathfinding.finder.breadth_first import BreadthFirstFinder

def get_turning_points(pathlist):
    outlist=[]
    outlist.append((pathlist[0], get_starting_destination(pathlist)))
    for i in range(len(pathlist)):
        try:
            if pathlist[i][1]==pathlist[i-1][1] and pathlist[i][1]<pathlist[i+1][1]:
                outlist.append((pathlist[i], 'S'))
            elif pathlist[i][1]==pathlist[i-1][1] and pathlist[i][1]>pathlist[i+1][1]:
                outlist.append((pathlist[i], 'N'))
            elif pathlist[i][0]==pathlist[i-1][0] and pathlist[i][0]>pathlist[i+1][0]:
                outlist.append((pathlist[i], 'W'))
            elif pathlist[i][0]==pathlist[i-1][0] and pathlist[i][0]<pathlist[i+1][0]:
                outlist.append((pathlist[i], 'E'))
        except:
            pass
    return outlist

def get_starting_destination(pathlist):
    if pathlist[0][0]>pathlist[1][0]:
        return 'W'
    elif pathlist[0][1]>pathlist[1][1]:
        return 'N'
    elif pathlist[0][0]<pathlist[1][0]:
        return 'E'
    else:
        return 'S'
    
def straighten_corners(pathlist):
    for i in range(len(pathlist)):
        try:
            if pathlist[i-2][0]<pathlist[i][0] and pathlist[i-2][1]>pathlist[i][1] and pathlist[i+2][0]>pathlist[i][0] and pathlist[i+2][1]<pathlist[i][1]:
                pathlist[i]=(pathlist[i][0]+1,pathlist[i][1]+1)
            elif pathlist[i-2][0]>pathlist[i][0] and pathlist[i-2][1]<pathlist[i][1] and pathlist[i+2][0]<pathlist[i][0] and pathlist[i+2][1]>pathlist[i][1]:
                pathlist[i]=(pathlist[i][0]+1,pathlist[i][1]+1)
            elif pathlist[i-2][0]>pathlist[i][0] and pathlist[i-2][1]>pathlist[i][1] and pathlist[i+2][0]<pathlist[i][0] and pathlist[i+2][1]<pathlist[i][1]:
                pathlist[i]=(pathlist[i][0]-1,pathlist[i][1]-1)
            elif pathlist[i-2][0]<pathlist[i][0] and pathlist[i-2][1]<pathlist[i][1] and pathlist[i+2][0]>pathlist[i][0] and pathlist[i+2][1]>pathlist[i][1]:
                pathlist[i]=(pathlist[i][0]-1,pathlist[i][1]-1)
        except:
            pass

def mazesolve():
    with open('maze_example.txt') as file:
        maze=file.read()

    maze=maze.strip().split('\n')

    matrix=[]
    for i in maze:
        matrix.append(list(i))

    grid = Grid(matrix=matrix)

    start = grid.node(14, 14)
    end = grid.node(72, 16)

    finder = BreadthFirstFinder(diagonal_movement=DiagonalMovement.never)
    path, runs = finder.find_path(start, end, grid)

    path.append((0,0))

    straighten_corners(path)
    return get_turning_points(path)
