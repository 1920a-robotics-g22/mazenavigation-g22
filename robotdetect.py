import numpy as np
import cv2



try:
  file = open('trackbar_defaults.txt','r')
  values = []

  for line in file:
    values.append(int(line))
    
  lH = values[0]
  lS = values[1]
  lV = values[2]
  hH = values[3]
  hS = values[4]
  hV = values[5]

except:


  lH = 125
  lS = 125
  lV = 125
  hH = 255
  hS = 255
  hV = 255

# open the camera
cap = cv2.VideoCapture(0)
def updateValueh(newh):

  global lH
  lH=newh
  
  return
def updateValues(news):

  global lS
  lS=news

  return

def updateValuev(newv):

  global lV
  
  lV=newv
  
  return
def updateValueH(newH):

  global hH
 
  hH=newH
  
  return
def updateValueS(newS):

  global hS
 
  hS=newS
  
  return
def updateValueV(newV):

  global  hV
  
  hV=newV
  return


    
blobparams = cv2.SimpleBlobDetector_Params()

blobparams.filterByArea = False
blobparams.filterByCircularity = False
blobparams.filterByInertia = False
blobparams.filterByConvexity = False
blobparams.minDistBetweenBlobs = 200

detector = cv2.SimpleBlobDetector_create(blobparams)

cv2.namedWindow("Output")

# Attach a trackbar to a window named 'Output'
cv2.createTrackbar("lH", "Output", lH, 255, updateValueh)
cv2.createTrackbar("lS", "Output", lS, 255, updateValues)
cv2.createTrackbar("lV", "Output", lV, 255, updateValuev)
cv2.createTrackbar("hH", "Output", hH, 255, updateValueH)
cv2.createTrackbar("hS", "Output", hS, 255, updateValueS)
cv2.createTrackbar("hV", "Output", hV, 255, updateValueV)


while True:
  
 
  #read the image from the camera
  ret, frame = cap.read()

  frame = cv2.cvtColor(frame,cv2.COLOR_BGR2HSV)

  
  lowerLimits = np.array([lH, lS, lV])
  upperLimits = np.array([hH, hS, hV])

  # Our operations on the frame come here
  thresholded = cv2.inRange(frame, lowerLimits, upperLimits)
  outimage = cv2.bitwise_and(frame, frame, mask = thresholded)

  

  
  thresholded = cv2.bitwise_not(thresholded)
  cv2.imshow('Output', thresholded)
  keypoints = detector.detect(thresholded)
  imgcop=frame.copy()
  imgcop = cv2.drawKeypoints(imgcop, keypoints, np.array([]), (0,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

  if len(keypoints) > 0:
        for keypoint in keypoints:
            coord=keypoint.pt
            coord=list(coord)
            print(coord[0],coord[1])
              
  cv2.imshow('Original', imgcop)
 
 
 
  # Quit the program when 'q' is pressed
  if cv2.waitKey(1) & 0xFF == ord('q'):
    file = open('trackbar_defaults.txt','w+')
    
    file.write(str(lH) + "\n")
    file.write(str(lS) + "\n")
    file.write(str(lV) + "\n")
    file.write(str(hH) + "\n")
    file.write(str(hS) + "\n")
    file.write(str(hV) + "\n")
    file.close() 
    break

# When everything done, release the capture
print('closing program')
cap.release()
cv2.destroyAllWindows()
