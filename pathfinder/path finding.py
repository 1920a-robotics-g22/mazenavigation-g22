from pathfinding.core.diagonal_movement import DiagonalMovement
from pathfinding.core.grid import Grid
from pathfinding.finder.breadth_first import BreadthFirstFinder

with open('maze_example.txt') as file:
    maze=file.read()

maze=maze.strip().split('\n')

matrix=[]
for i in maze:
    matrix.append(list(i))

print(matrix)

grid = Grid(matrix=matrix)

start = grid.node(14, 14)
end = grid.node(73, 16)

finder = BreadthFirstFinder(diagonal_movement=DiagonalMovement.never)
path, runs = finder.find_path(start, end, grid)

print(grid.grid_str(path=path, start=start, end=end))

with open('maze_solution.txt', 'w') as output:
    output.write(str(path))