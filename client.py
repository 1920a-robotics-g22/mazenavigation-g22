#!/usr/bin/python           # This is client.py file

import socket
import easygopigo3 as go
# Import time
import time
myRobot = go.EasyGoPiGo3()
myRobot.set_speed(100)            # Import socket module

s = socket.socket()         # Create a socket object
host = '172.19.36.31' # Get local machine name
port = 8095            # Reserve a port for your service.

s.connect((host, port))
while True:
    n=s.recv(1024)
    print (n)
                   # Close the socket when done
    if n==b'close':
        break
    elif n==b'Thank you for connecting':
        print('good')
    elif n==b'frwd':
        print('Forward')
        myRobot.forward()
        time.sleep(2)
        myRobot.stop()
    elif n==b'back':
        print ('Backward')
        myRobot.backward()
        time.sleep(2)
        myRobot.stop()
    elif n==b'right':
        print ('Right')
        myRobot.right()
        time.sleep(2)
        myRobot.stop()
    elif n==b'left':
        print ('Left')
        myRobot.left()
        time.sleep(2)
        myRobot.stop()
    else:
        print ('please, repeat')