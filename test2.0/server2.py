#!/usr/bin/python           # This is server.py file

import socket               # Import socket module
import json
import mazefunctions as maze
import cv2
import cv2.aruco as aruco
import arucofunctions as af

s = socket.socket()         # Create a socket object
s.bind(('192.168.43.199', 8080)) # Get local machine name

s.listen(5)                 # Now wait for client connection.

# here comes a code of maze solving
#output - two arrays
cap = cv2.VideoCapture(1)

cap.set(cv2.CAP_PROP_FRAME_WIDTH, 640)
cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 360)

#assemble the maze, press q to get the maze picture
while True:
    ret, frame1 = cap.read()
    cv2.imshow('Frame1', frame1)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        m=maze.maze2binary(frame1)
        break


#put the robot in the maze, press q to get starting point of the robot for 
while True:
    ret, frame2 = cap.read()
    cv2.imshow('Frame2', frame2)
    corners=af.initaruco(frame2)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        start=af.getcoords(corners)
        break

#print("maze")
#print(m)
#print("start")
#print(int(start[1]+1),int(start[0]+1))
    
path = maze.mazesolve(m, (int(start[1]), int(start[0])),(200,180)) # consists of bin maze, start point, end point

points=[]
turns=[]
for el in path:
    points.append(el[0])
    turns.append(el[1])

#print("points")
#print(points)
#print("turns")
#print(turns)
c, addr = s.accept()     # Establish connection with client.
print ('Got connection from', addr)
data = json.dumps({"points": points, "turns": turns})
c.send(data.encode())
locate=str.encode('loc')
c.send(locate)

#THIS PART IS THE MAIN PROBLEM
#while True:
#      loc=af.getall(corners)
#      data = json.dumps({"loc": loc})
#      c.send(data.encode())
#      if loc==points[-1]:
#          closing=str.encode('close')
#          c.send(closing)
#          c.close()
#          break
#      else:
#          c.send(data.encode())

