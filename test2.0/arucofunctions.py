import cv2
import cv2.aruco as aruco
import math

cap = cv2.VideoCapture(0)

cap.set(cv2.CAP_PROP_FRAME_WIDTH, 640)
cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 360)

corners = []

def initaruco(frame): #setup function
    global corners
    #ret, frame = cap.read()
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    aruco_dict = aruco.Dictionary_get(aruco.DICT_4X4_50) #4x4 should give better result than 6x6 because squares are bigger
    parameters =  aruco.DetectorParameters_create()

    #aruco marker detector
    corners, ids, rejectedImgPoints = aruco.detectMarkers(gray, aruco_dict, parameters=parameters)
    return corners

def getcoords(corners): #returns only coordinates, needs initaruco()
    if len(corners) > 0:
        #calculating center coordinates
        #x = (corners[0][0][0][0] + corners[0][0][1][0] + corners[0][0][2][0] + corners[0][0][3][0]) / 4
        #y = (corners[0][0][0][1] + corners[0][0][1][1] + corners[0][0][2][1] + corners[0][0][3][1]) / 4
        
        #or use int values:
        x = int((corners[0][0][0][0] + corners[0][0][1][0] + corners[0][0][2][0] + corners[0][0][3][0]) / 4)
        y = int((corners[0][0][0][1] + corners[0][0][1][1] + corners[0][0][2][1] + corners[0][0][3][1]) / 4)
        
        return x, y

def getheading1(corners): #returns heading as it was before, needs initaruco()
    if len(corners) > 0:
        #choosing corners for angle calculation, 3 and 4 are two bottom ones so that robot facing north gives 0 degrees
        p1x = corners[0][0][0][0]
        p2x = corners[0][0][1][0]
        p3x = corners[0][0][2][0]
        p4x = corners[0][0][3][0]
        p1y = corners[0][0][0][1]
        p2y = corners[0][0][1][1]
        p3y = corners[0][0][2][1]
        p4y = corners[0][0][3][1]
        

        #angle/heading calculation as it was before, 1 corner at front left corner of the robot
        #averages lines between 1 and 2, 3 and 4
        dx = (p3x - p4x)+(p2x - p1x)/2
        dy = (p4y - p3y)+(p1y-p2y)/2
        
        rads = math.atan2(dy,dx)
        degs = math.degrees(rads)
        
        return degs

def getheading2(corners): #returns heading, uses longer line for angle calculations
    if len(corners) > 0:
        #choosing corners for angle calculation, this is for diagonal aruco marker, 1. corner at the front
        p2x = corners[0][0][1][0]
        p4x = corners[0][0][3][0]
        p2y = corners[0][0][1][1]
        p4y = corners[0][0][3][1]
        
        #angle/heading calculation, marker on robot diagonally, 1 corner at the front. longer line - more accurate angle
        dx = p2x - p4x
        dy = p4y - p2y
        
        rads = math.atan2(dy,dx)
        degs = math.degrees(rads)
        
        return degs
    
def getall(corners): #gets corners and heading, needs initaruco()
    if len(corners) > 0:
        #calculating center coordinates
        x = int((corners[0][0][0][0] + corners[0][0][1][0] + corners[0][0][2][0] + corners[0][0][3][0]) / 4)
        y = int((corners[0][0][0][1] + corners[0][0][1][1] + corners[0][0][2][1] + corners[0][0][3][1]) / 4)
        
        #choosing corners for angle calculation, this is for diagonal aruco marker, 1. corner at the front
        p2x = corners[0][0][1][0]
        p4x = corners[0][0][3][0]
        p2y = corners[0][0][1][1]
        p4y = corners[0][0][3][1]
        
        #angle/heading calculation, marker on robot diagonally, 1 corner at the front. longer line - more accurate angle
        dx = p2x - p4x
        dy = p4y - p2y
        
        rads = math.atan2(dy,dx)
        degs = round(math.degrees(rads),2)
        
        return x, y, degs

def arucomarker(): #everything in one function
    ret, frame = cap.read()
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    aruco_dict = aruco.Dictionary_get(aruco.DICT_4X4_50) #4x4 should give better result than 6x6 because squares are bigger
    parameters =  aruco.DetectorParameters_create()

    #aruco marker detector
    corners, ids, rejectedImgPoints = aruco.detectMarkers(gray, aruco_dict, parameters=parameters)
    
    if len(corners) > 0:
        #calculating center coordinates
        x = (corners[0][0][0][0] + corners[0][0][1][0] + corners[0][0][2][0] + corners[0][0][3][0]) / 4
        y = (corners[0][0][0][1] + corners[0][0][1][1] + corners[0][0][2][1] + corners[0][0][3][1]) / 4
        
        #choosing corners for angle calculation, this is for diagonal aruco marker, 1. corner at the front
        p2x = corners[0][0][1][0]
        p4x = corners[0][0][3][0]
        p2y = corners[0][0][1][1]
        p4y = corners[0][0][3][1]
        
        #angle/heading calculation, marker on robot diagonally, 1 corner at the front. longer line - more accurate angle
        dx = p2x - p4x
        dy = p4y - p2y
        
        #rads = math.atan2(dy,dx)
        degs = round(math.degrees(rads),2)
        
        return x, y, degs

#while True:
#    print(arucomarker())
#    initaruco()
#    print(getcoords(corners))
#    #print(getheading1(corners))
#    #print(getheading2(corners))
#    print(getall(corners))
##    
#    if cv2.waitKey(1) & 0xFF == ord('q'):
#        break


cap.release()
cv2.destroyAllWindows()


