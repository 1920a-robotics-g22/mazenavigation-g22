#performs edge detection and morphological operations according to values previously tested and set up in setup.py
#takes original frame of maze as input, black and white file as output

import cv2
import numpy as np
import os



my_path = "trackbar_values.txt"

if os.path.exists(my_path) and os.path.getsize(my_path) > 0:
  with open(my_path, "r") as configfile:      
    lines = configfile.readlines()
    scale_percent = int(lines[0])
    lowthreshold = int(lines[1])
    dilationsize = int(lines[2])
    erosionsize = int(lines[3])     
else:
  scale_percent = 10
  lowthreshold = 25
  dilationsize = 1
  erosionsize = 1


#open image file
inputfile = input("Please enter file name used in setup.py: ")

while os.path.exists(inputfile) == False:
  inputfile = input("Please enter correct file name (with extension): ")
  
i = cv2.imread(inputfile)




while True:
    #input image resolution settings
    width = int(i.shape[1] * scale_percent / 3 / 100)
    height = int(i.shape[0] * scale_percent / 3 / 100)
    dim = (width, height)
    img = cv2.resize(i, dim, interpolation = cv2.INTER_AREA)

    #convert to grayscale
    gray_scale = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    
    #blur
    img_blur = cv2.GaussianBlur(gray_scale,(5,5),0)
    
    #Canny edge detection
    detected_edges = cv2.Canny(img_blur, lowthreshold, lowthreshold*3, 3)
    
    #morphological operations
    dilate_mask = np.ones((dilationsize, dilationsize), np.uint8)
    dilate_result = cv2.dilate(detected_edges, dilate_mask, iterations = 1)
    
    erode_mask = np.ones((erosionsize, erosionsize), np.uint8)
    erosion_result = cv2.erode(dilate_result, erode_mask, iterations = 1)
    
    outputfile = input("Please enter output file name (without extension): ")
    cv2.imwrite(str(outputfile + ".png"), erosion_result)
    print("Saved output file under the name " + outputfile + ".png")
    break

    
print("Closing program")

cv2.destroyAllWindows()



