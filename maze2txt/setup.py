#setup program for setting up and testing effective values for frame transformation to black and white image
#takes takes original frame of maze as input, outputs trackbar_values.txt file for using in edgedetection.py

import cv2
import numpy as np
import os



valuesfile = "trackbar_values.txt"

if os.path.exists(valuesfile) and os.path.getsize(valuesfile) > 0:
  with open(valuesfile, "r") as configvalues:      
    lines = configvalues.readlines()
    scale_percent = int(lines[0])
    lowthreshold = int(lines[1])
    dilationsize = int(lines[2])
    erosionsize = int(lines[3])     
else: #these values output a 60x80px image from demo jpg's 
  scale_percent = 6
  lowthreshold = 50
  dilationsize = 5
  erosionsize = 5


def updatescale(new_value):
  global scale_percent
  scale_percent = new_value
  if scale_percent == 0:
      scale_percent = 1
  return

def updatelowthreshold(new_value):
  global lowthreshold
  lowthreshold = new_value
  return

def update_dilationsize(new_value):
  global dilationsize
  dilationsize = new_value
  if dilationsize%2==0:
    dilationsize+=1
  return

def update_erosionsize(new_value):
  global erosionsize
  erosionsize = new_value
  if erosionsize%2==0:
    erosionsize+=1
  return

#open image file
filename = input("Please enter file name: ")

while os.path.exists(filename) == False:
  filename = input("Please enter correct file name (with extension: ")
  
i = cv2.imread(filename)


# create a window named trackbars
cv2.namedWindow("Trackbars")
cv2.resizeWindow("Trackbars", 400, 170)

# add trackbars to that window
cv2.createTrackbar("scale percent", "Trackbars", scale_percent, 100, updatescale)
cv2.createTrackbar("low threshold", "Trackbars" , lowthreshold, 100, updatelowthreshold)
cv2.createTrackbar("dilation kernel", "Trackbars", dilationsize, 40, update_dilationsize)
cv2.createTrackbar("erosion kernel", "Trackbars", erosionsize, 40, update_erosionsize)


while True:
    #set input image resolution, after real-life testing should be set manually, this is just for simulation images, original images huge so that's why there's that /3 as well
    width = int(i.shape[1] * scale_percent / 3 / 100)
    height = int(i.shape[0] * scale_percent / 3 / 100)
    dim = (width, height)
    img = cv2.resize(i, dim, interpolation = cv2.INTER_AREA)

    #convert to grayscale
    gray_scale = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    
    #blur before edge detection
    img_blur = cv2.GaussianBlur(gray_scale,(5,5),0)
    
    #Canny edge detection, lowthreshold varies in different lighting (higher low value will not work in darker situations), min max ratio set to 1:3
    detected_edges = cv2.Canny(img_blur, lowthreshold, lowthreshold*3, 3)
    cv2.imshow("detected_edges", detected_edges)
    
    #dilation, used for connecting-merging both edges, inside and outside
    dilate_mask = np.ones((dilationsize, dilationsize), np.uint8)
    dilate_result = cv2.dilate(detected_edges, dilate_mask, iterations = 1)
    cv2.imshow("dilate", dilate_result)
    
    #erosion after dilation, used for eroding those 2x width edges back to normal size
    #these two together make up closing morphological operation, but in some cases it's good to test them separately
    erode_mask = np.ones((erosionsize, erosionsize), np.uint8)
    erosion_result = cv2.erode(dilate_result, erode_mask, iterations = 1)
    #if edges were connected and then eroded to normal size then this should show walls as white and other area as black
    cv2.imshow("erosion", erosion_result)
    
    #prints output image resolution
    print(detected_edges.shape)
        
    #quit the program when 'q' is pressed
    if cv2.waitKey(1) & 0xFF == ord('q'):
      break

    
print("Closing program")


with open(valuesfile, "w") as configvalues:
  configvalues.write(str(scale_percent) + "\n")
  configvalues.write(str(lowthreshold) + "\n")
  configvalues.write(str(dilationsize) + "\n")
  configvalues.write(str(erosionsize) + "\n")


cv2.destroyAllWindows()


