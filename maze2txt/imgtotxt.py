#turns output image from edgedetection.py into 0's and 1's in a txt file

import cv2
import numpy as np
import sys
import os

np.set_printoptions(threshold=sys.maxsize)


#open image file
inputfile = input("Please enter edgedetection.py output file name: ")

while os.path.exists(inputfile) == False:
  inputfile = input("Please enter correct file name (with extension): ")
  
#inverts black and white
img = cv2.imread(inputfile, 0)
img = cv2.bitwise_not(img)

#convert array range to be from 0 to 1 (default is 0 to 255)
floatvalues = img / 255

#convert elements to integers, 0 as walls and 1 as passable area
mazearray = floatvalues.astype(int)


mazenumpy = np.array(mazearray)
#print(mazenumpy)

#print(mazenumpy.shape)

h, w = mazenumpy.shape

#if needed then here it can print out numpy 2d array, elements are integers, 0 and 1
#numpytoarray = np.array_str(mazenumpy)
#outputnumpy = "numpyarray.txt"

#with open(outputnumpy, "w") as output:
#    output.write(numpytoarray)

outputfile = input("Please enter output file name (without extension): ")
outputtxt = outputfile + ".txt"
with open(outputtxt, "w") as output:
    for r in range(h):
        for c in range(w):
            element = mazenumpy.item(r,c)
            output.write(str(element))
        output.write("\n")
        
print("Saved output file under the name " + outputfile + ".txt")        

print("Closing program")