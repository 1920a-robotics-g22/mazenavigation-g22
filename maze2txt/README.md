### Readme file for maze to text solution

#### Preliminary solution consists of 3 main parts:
1. setup.py
2. edgedetection.py
3. imgtotext.py

setup.py - here you can use trackbars to change transformation values. Tools involve Canny edge detection and morphological operations (dilation and erosion) which are mainly to be used as a closing operation (same values for both of them). This program uses trackbar_values.txt file.  
edgedetection.py - this takes the input image and values from trackbar_values.txt and converts the image to a black and white .png file according to the values.  
imgtotext.py - this program converts output from edgedetection.py into .txt file which consists of 0's and 1's. 1 is passable area and 0 is wall element. Used in maze solution algorithm.

#### Files used in the project:
1. valuesetup.py
2. mazefunctions.py

valuesetup.py - program to test out best values of the real maze with webcam and then use these values for maze2binary function  
mazefunctions.py - contains the final maze2binary function

thinning.py is a program which does Zhang-Suen thinning algorithm ([source](https://rosettacode.org/wiki/Zhang-Suen_thinning_algorithm)). It might be useful for robot movement/route planning (edit: didn't use).

Also added are example photos (maze1.jpg, maze2.jpg etc) which are taken in different light conditions and are used for testing.

Folder "additional images" contains pictures of the process with comments to describe the idea.