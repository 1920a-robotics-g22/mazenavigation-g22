#function for getting binary maze from one frame of webcam feed
#might need to lower resolution for calculation and then upscale the resolution coordinates

import numpy as np
import cv2
import os

cap = cv2.VideoCapture(0)

cap.set(cv2.CAP_PROP_FRAME_WIDTH, 640)
cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 360)

def maze2binary():
    valuesfile = "maze_values.txt"
    if os.path.exists(valuesfile) and os.path.getsize(valuesfile) > 0:
        with open(valuesfile, "r") as configvalues:      
            lines = configvalues.readlines()
            lowthreshold = int(lines[0])
            dilationsize = int(lines[1])
            erosionsize = int(lines[2])     
    else:
        lowthreshold = 50
        dilationsize = 23
        erosionsize = 23
        
    ret, frame = cap.read()
    
    #convert to grayscale
    gray_scale = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    #blur
    img_blur = cv2.GaussianBlur(gray_scale,(5,5),0)
    
    #Canny edge detection
    detected_edges = cv2.Canny(img_blur, lowthreshold, lowthreshold*3, 3)
    
    #morphological operations
    dilate_mask = np.ones((dilationsize, dilationsize), np.uint8)
    dilate_result = cv2.dilate(detected_edges, dilate_mask, iterations = 1)
    
    erode_mask = np.ones((erosionsize, erosionsize), np.uint8)
    erosion_result = cv2.erode(dilate_result, erode_mask, iterations = 1)
    
    #inverts black and white
    img = cv2.bitwise_not(erosion_result)
    
    #convert array range to be from 0 to 1 (default is 0 to 255)
    floatvalues = img / 255

    #convert elements to integers, 0 as walls and 1 as passable area
    mazearray = floatvalues.astype(int)
    
    mazenumpy = np.array(mazearray)
    
    h, w = mazenumpy.shape
    outputtxt = "output.txt"
    with open(outputtxt, "w") as output:
        for r in range(h):
            for c in range(w):
                element = mazenumpy.item(r,c)
                output.write(str(element))
            output.write("\n")
    
#maze2binary()
