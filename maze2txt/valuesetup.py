#program to test out best values of the real maze with webcam and then use these values for maze2bin function

import cv2
import numpy as np
import os

cap = cv2.VideoCapture(0)

cap.set(cv2.CAP_PROP_FRAME_WIDTH, 640)
cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 360)

valuesfile = "maze_values.txt"

if os.path.exists(valuesfile) and os.path.getsize(valuesfile) > 0:
  with open(valuesfile, "r") as configvalues:      
    lines = configvalues.readlines()
    lowthreshold = int(lines[0])
    dilationsize = int(lines[1])
    erosionsize = int(lines[2])     
else: 
  lowthreshold = 50
  dilationsize = 5
  erosionsize = 5

def updatelowthreshold(new_value):
  global lowthreshold
  lowthreshold = new_value
  return

def update_dilationsize(new_value):
  global dilationsize
  dilationsize = new_value
  if dilationsize%2==0:
    dilationsize+=1
  return

def update_erosionsize(new_value):
  global erosionsize
  erosionsize = new_value
  if erosionsize%2==0:
    erosionsize+=1
  return


# create a window named trackbars
cv2.namedWindow("Trackbars")
cv2.resizeWindow("Trackbars", 400, 170)

# add trackbars to that window
cv2.createTrackbar("low threshold", "Trackbars" , lowthreshold, 100, updatelowthreshold)
cv2.createTrackbar("dilation kernel", "Trackbars", dilationsize, 40, update_dilationsize)
cv2.createTrackbar("erosion kernel", "Trackbars", erosionsize, 40, update_erosionsize)


while True:
    ret, frame = cap.read()

    #convert to grayscale
    gray_scale = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    
    #blur before edge detection
    img_blur = cv2.GaussianBlur(gray_scale,(5,5),0)
    
    #Canny edge detection, lowthreshold varies in different lighting (higher low value will not work in darker situations), min max ratio set to 1:3
    detected_edges = cv2.Canny(img_blur, lowthreshold, lowthreshold*3, 3)
    cv2.imshow("detected_edges", detected_edges)
    
    #dilation, used for connecting-merging both edges, inside and outside
    dilate_mask = np.ones((dilationsize, dilationsize), np.uint8)
    dilate_result = cv2.dilate(detected_edges, dilate_mask, iterations = 1)
    cv2.imshow("dilate", dilate_result)
    
    #erosion after dilation, used for eroding those 2x width edges back to normal size
    #these two together make up closing morphological operation, but in some cases it's good to test them separately
    erode_mask = np.ones((erosionsize, erosionsize), np.uint8)
    erosion_result = cv2.erode(dilate_result, erode_mask, iterations = 1)
    #if edges were connected and then eroded to normal size then this should show walls as white and other area as black
    cv2.imshow("erosion", erosion_result)
        
    #quit the program when 'q' is pressed
    if cv2.waitKey(1) & 0xFF == ord('q'):
      break

print("Closing program")

with open(valuesfile, "w") as configvalues:
  configvalues.write(str(lowthreshold) + "\n")
  configvalues.write(str(dilationsize) + "\n")
  configvalues.write(str(erosionsize) + "\n")

cv2.destroyAllWindows()



