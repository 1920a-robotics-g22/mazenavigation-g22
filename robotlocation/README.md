### Readme file for robot location

* arucofunctions.py - different functions to be used (or not) in the main program
* robotlocation.py - this program was used for outputting robot's coordinates and it's heading
* robot_marker.jpg - 4x4 ArUco marker to be printed out and put on the robot
* finish_marker.jpg - 4x4 ArUco marker to be printed out and put at the end of the maze
* arucoguide.png - corner numbers and marker installing direction