## Maze navigation with GoPiGo3

Group members:  
Jaan Kristjan Lepp  
Gerda M�ndmaa  
Irina Borovko  
Vladimir Lijepa  

#### Overview
1. Making Gopigo move from laptop
	1. Sending a simple forward command
	2. Making any code from any lab involving GoPiGo work calling from laptop (to focus on the remote control part and not the code itself)
	3. Implementing remote control to the final code

2. Gopigo movements
	1. Making robot follow the black line (or white)
	2. Making robot turn and find the line again (or can get directions from the list of commands)
	3. Making robot turn the right direction to move towards the exit (get directions from the list of commands)

3. Processing maze image from the top
	1. Transforming camera image into the map
	2. Putting the Gopigo on the map
	3. Getting the direction Gopigo is facing

4. Maze solution
	1. Figuring out the algorithm to solve mazes (start to finish)
	2. Upgrade the algorithm to be able to find exit from any point of the maze
	3. Making a function which gets the maze map and returns the list of commands (etc [right, left, left, right])

1 and 2: Discuss the remote control part and figure out how much (if at all) does 3 need to change the code for it to work in remote control mode.  
3 and 4: Discuss how the map should look like so that 4 can apply its algorithm to it.

We also have to make sure that changing light conditions do not affect the performance of the robot.

#### Schedule
| Week | Dates | Task | Hours | Total |
| :--: | :--: | -- | :--: | :--: |
| 1 | 11.-17. | Project plan | 5 | 5 |
|  |  | Research | 5 | 10 |
| 2 | 18.-24. | Finalizing the logic behind picture-to-map | 10 | 20 |
|  |  | Standard computer map for testing  | 5 | 25 |
|  |  | Movement basics | 5 | 30 |
| 3 | 25.-1. | Working towards a working concept | 20 | 50 |
|  |  | Robot to map | 20 | 70 |
|  |  | Making the maze | 5 | 75 |
| 4 | 2.-8. | Proof of concept video | 20 | 95 |
|  |  | Adding movements | 10 | 105 |
| 5 | 9.-15. | Improving movements/control | 15 | 120 |
|  |  | Improving detection algorithm  | 15 | 135 |
| 6 | 16.-22. | Project poster | 10 | 145 |
|  |  | Improving solving algorithm | 10 | 155 |
| 7 | 23.-29. | Problem solving | 10 | 165 |
| 8 | 30.-5. | Buffer | 20 | 185 |
| 9 | 6.-13. | Final setup | 10 | 195 |
|  |  | Presentation  | 10 | 205 |


#### Component list
| Item | Link to the item | You will provide | Need from instructors | Total |
| -------------- | -------------- | --------------: | ----------------: | :----------------: |
| GoPiGo3 | https://www.dexterindustries.com/gopigo3/ | 0 | 1 | 1 |
| LiPo battery | - | 0 | 1 | 1 |
| Micro SD card | - | 0 | 1 | 1 |
| Laptop | - | 0 | 1 | 1 |
| External webcam | - | 0 | 1 | 1 |
| A0 paper | - | 1 | 0 | 1 |
| Set of black lineblocks (custom made) | - | 1 | 0 | 1 |


#### Challenges and Solutions
Task 1. Irina  
Main challenge was to understand how exactly the data is going to be transferred to the robot. 
Jaan Kristjan suggested bluetooth, and Ragnar - sockets. 
Eventually, I have stopped on the sockets. 
The communication is now working fine. 
I am thinking about automating it further, so the user doesn't have to write the address manually. 
Right now, as a proof of concept, robot moves from the input commands from the terminal. 
The problem might be that there are going to be two types of inputs. 
One in the beginning of the run with the path and then constantly renewable one with the location. 
In the project I am thinking of getting directions and points each separately and append them to two lists. I still have to think through how it will know that lists are finished and the next input is a location. Probably some input additional like "stop" will indicate it. Next inputs are going to be coordinates from the blob detection(robot). By renewing them we will know when the robot has to stop and turn. 
My concern is too many loops in the loop. I will try to think it through when I will see the final code from the task 2. 

Task 2. Gerda  
My task has to do with all robot movements including staying on lane, no cutting corners, no going out of bounds, turning at wrong angles.  
For input there will be list of commands and turning points. For now I manually tried to insert some points and commands to some list to see how the movement works out. Challenging is to make the robot go to a specific point: it is not too accurate right now. 
If it drives along x-axis I need to make sure it does not drift off on the y-axis. And then turning to a right angle.  
I need information about robot location to know if it reached destination point so it can take the next point and next command. Location is also necessary to know if the end of the maze has been reached.
Right now I am using blob detection for location of robot but I am looking into Aruco markers if maybe it will make things easier regarding of where the front of the robot is and the centerpoint.
Future challenges include making robot drive straight, implementing Aruco for angle situation and location.  

Task 3. Jaan Kristjan  
This task was separated into two parts: 1) translating a frame of the maze into something that the solution algorithm could understand and 2) implementing a solution which puts the robot on the map i.e. gives its coordinates and also its heading.  
I started with the first one (maze2txt). Main challenge was finding a good function to use for this task which involved a lot of testing with a mockup A4 size maze.  
I went through thresholding + morphological operations (maze2array_obsolete) which worked fine but isn't as robust as edge detection so that's why I decided to use that one instead. I also did tests with line detection and corner detection algorithms but they usually made things more difficult without giving much back.
Final solution was using edge detection, it worked well in different lighting conditions and to make its output into something Vladimir's algorithm could use was quite straightforward - just 0 and 1 pixels representing passable area and walls.  
At this point, it's output is configurable, for example it might be good to just use dilation to make available corridors really narrow for the solution program so the routes between solution points would be basically in the center line anyway. I also found a thinning algorithm (Zhang-Suen) which does provide 1px wide routes but intersections get diagonal and we don't want that.  
Initially, for getting robot's location and to get the movement programming started, Irina set up getting robot's coordinates with blob detection.  
Getting its heading seemed more difficult and one of the options was to program robot's last direction into the movement function.  
Ragnar introduced Aruco markers to us and the solution (robotlocation - hasn't been implemented yet) wasn't so difficult at all: using marker's 4 corners the program calculates its center coordinate and using two of the coordinates as points of a line I could calculate the angle of it and therefore the robot's. I might use the other two as well and average the angle value, but let's see if it's needed.  

Task 4. Vladimir  
Main challenge of this task was finding the most effective maze solving algorithm specifically for our project. Since the task itself states, that algorithm has to find the shortest path, many algorithms such as wall following did not make sense. 
First thing I have tried to implement was A Star, however, it proved to be a bad decision since the paths in out maze are wider than 1px and A* tends to cut the edges too much and move diagonally. 
The algorithm i settled on was breadth first finder. It prefered moving in much more straight lines, although, still cuts corners. 
I am thinking, that cutting corners is the nature of every algorithm, which wants to find the shortest path, so it might be something we have to work around. 
Right now algorithm outputs the whole path of solving the maze. With that said, alternating the output for something more suitable will not be a problem the very moment we figure out what exactly the output should be. 
My main focus transforiming the output into the list of directions with turning points, because thats what our team settled on after some discussions.